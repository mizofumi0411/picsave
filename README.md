#なにこれ
2chのスレからjpgやらgifやらpngやらのファイルを一括保存するやつ。
保存対象の拡張子は画像以外もOK。
2chのスレは、複数登録でき各スレからファイルを落とす。保存済みの画像がある場合はスキップするので保存されないです。結構便利✌('ω')

#インストール
####必要なパッケージ類を入れる
picsaveディレクトリを開いて

	$ bundle install

####PATHを通す
~/.zshrc にPATHを通す　※zsh以外のシェルでもOK

	export PATH="$PATH:<picsaveのパス>"

追記したPATHを適用
	
	$ source ~/.zshrc
	
#オプション
- add	=> スレのURLを追加
- list	=> 登録されたURLを表示
- del 	=> 登録されたURLを削除

#使い方
###URL(スレ)追加
URLリストに追加します
	$ svpic add http://hoge.2ch.net/test/read.cgi/hoge/1234567890
	
###URL(スレ)確認
URLリストを確認します
	$ svpic list
	#=>
	"0 : http://hoge.2ch.net/test/read.cgi/hoge/1234567890"
	"1 : http://hoge.2ch.net/test/read.cgi/hoge/9876543210"
	
###URL(スレ)削除
URLリストから指定した番号のURLを削除します
	$ svpic del 0
	#=>
	"0 : http://hoge.2ch.net/test/read.cgi/hoge/9876543210"
	
##URL(スレ)初期化
URLリストに登録されているURLをすべて削除します

	$ svpic init
	
###画像保存の開始
URLリストに登録されているサイト内から画像を保存します

	$ svpic
	#=>
	1 / 6 => saved! 
	2 / 6 => saved! 
	3 / 6 => saved! 
	4 / 6 => saved! 
	5 / 6 => saved! 
	6 / 6 => saved!
	
###画像保存のスキップ
既に保存されている画像は保存をスキップし新規分の画像を保存します

	$ svpic
	#=>
	1 / 7 => skipped!
	2 / 7 => skipped!
	3 / 7 => skipped!
	4 / 7 => skipped!
	5 / 7 => skipped!
	6 / 7 => skipped!
	7 / 7 => saved!

##動作確認環境
 - OS
   	- Mac OS X 10.8.3
 - Ruby (rbenv)
 	- ruby 1.9.3p429
 - Shell
 	- zsh 4.3.11
