# encoding : utf-8
require 'open-uri'
require 'rubygems'
require 'nokogiri'

class Picture
	def initialize(_url)
		@url = _url
		@extension = File.basename(_url)
		@esc_red_start = "\e[31;1m"
		@esc_green_start = "\e[32;1m"
		@esc_yellow_start = "\e[33;1m"
		@esc_blue_start = "\e[34;1m"
		@esc_magenta_start = "\e[35;1m"
		@esc_cyan_start = "\e[36;1m"
		@esc_white_start = "\e[37;1m"
		@esc_end = "\e[0m"
		#保存先のディレクトリ	
		@savepath = "/Volumes/SubHDD/Picture/2ch/"
	end

	def save_file()
		path = @savepath + @extension
		if File.exist?("#{path}") == false
			open(path, 'wb') do |file|
				begin
					open(@url) do |data|
						file.write(data.read)
						print "#{$nowcnt} / #{$maxcnt} => #{@esc_cyan_start}saved!#{@esc_end} \n"
						$okcnt += 1
					end
				rescue OpenURI::HTTPError => ex
					File.unlink path
					warn "#{$nowcnt} / #{$maxcnt} => #{@esc_red_start}" + ex.message + "#{@esc_end}"
					$ercnt += 1
				end
			end
		else
			$skipcnt += 1
			print "#{$nowcnt} / #{$maxcnt} => #{@esc_yellow_start}skipped!#{@esc_end} \n"
		end
		$nowcnt += 1
	end
end
