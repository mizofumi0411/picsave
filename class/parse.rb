class Parse
	def initialize(_url = 'http://www.google.com',_ua = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)')
		@url = _url
		@user_agent = _ua
		@extension = ['gif','jpg','png']
		@doc = Nokogiri::HTML( open(@url, 'User-Agent' => @user_agent))
		
	end

	def parse()
		a = Array.new()
		@doc.search('a').each do |link|
			ex = link[:href].split(".").last
			if @extension.include?(ex) == true
				a << link[:href].sub('ime.nu/','')
			end
		end
		return a
	end
end